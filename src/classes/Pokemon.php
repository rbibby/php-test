<?php
namespace classes;

use GuzzleHttp\Client;

class Pokemon {
    public $id;
    public $name;
    public $species;
    public $height;
    public $weight;
    public $abilities = [];

    private $client;

    public function __construct($client = null)
    {
        if (!is_null($client)) {
            $this->client = $client;
            return;
        }

        $this->client = new Client([
            'base_uri' => 'https://pokeapi.co/api/v2/pokemon/',
        ]);
    }

    /**
     * @return $this
     */
    public function loadAllDetails()
    {
        $response = $this->client->get($this->name);
        $pokemon = json_decode($response->getBody()->getContents());

        $this->id = $pokemon->id;
        $this->name = $pokemon->name;
        $this->weight = $pokemon->weight;
        $this->height = $pokemon->height;
        $this->species = $pokemon->species->name;
        foreach ($pokemon->abilities as $ability) {
            $this->abilities[] = $ability->ability->name;
        }

        return $this;
    }
}