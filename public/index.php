<?php
require('../vendor/autoload.php');
require('../src/classes/Pokemon.php');

use GuzzleHttp\Client;

$loader = new Twig_Loader_Filesystem('../src/twig-templates');
$twig = new Twig_Environment($loader, array(
    'debug' => true,
    'cache' => 'twig-cache',
));

$resultsFound = true;
$searchTerm = isset($_GET['search']) ? strtolower($_GET['search']) : '';

$client = new Client([
    'base_uri' => 'https://pokeapi.co/api/v2/pokemon/',
]);

try {
    $response = $client->get($searchTerm, ['limit' => 21]);
    $response = json_decode($response->getBody()->getContents());

    $pokemons = [];
    $pokemons[] = $response;
    if (isset($response->results)) { // different response structure when searching through the API
        $pokemons = $response->results;
    }

    $pokemonList = [];
    foreach ($pokemons as $pokemon) {
        $newObject = new \classes\Pokemon;
        $newObject->name = $pokemon->name;
        $newObject->loadAllDetails();

        $pokemonList[] = $newObject;
    }
} catch (Exception $e) {
    $resultsFound = false;
}

try {
    $template = $twig->loadTemplate('index.twig');
    echo $template->render([
        'pokemons' => $pokemonList,
        'resultsFound' => $resultsFound,
        'searchBox' => $searchTerm,
        'pagination' => [
            'next' => $response->next,
            'previous' => $response->previous,
        ]
    ]);
} catch (\Exception $e) {
    echo "An error occurred, please try again later";
}